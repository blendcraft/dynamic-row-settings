/**
 * Dynamic Row Settings plugin for Craft CMS
 *
 * Dynamic Row Settings JS
 *
 * @author    Kyle Andrews
 * @copyright Copyright (c) 2018 Kyle Andrews
 * @link      http://www.gamesbykyle.com
 * @package   DynamicRowSettings
 * @since     1.0.0
 */

 /***** Variables **************************************************************/
 let dcaBlocks = [
    {
        "matrixHandle": "dca",
        "blocks": [
            {
                "blockName": "Row Settings",
                "blockHandle": "rowSettings",
                "requireFirstSlot": true,
            },
        ],
    },
];
/******************************************************************************/

///////////////////////////////////// Popup Entry Editor \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
//Listen for ajax
//Used to get popup entries
$(window).ajaxComplete(function(){ checkForMatrixBlock(); });
//Validate our entry on save
function listenForPopupSave(matrixToCheck){
    let parent = $(matrixToCheck).parents('.body.elementeditor');
    $(parent).find('input.btn.submit').on('click', function(){ return popupValidator(matrixToCheck); });
}
//Check to see if this entry has a matrix block in it
function checkForMatrixBlock(){
$('.body.elementeditor .matrix.matrix-field').each(function(){
   //Make sure it's a DCA block
    if(validateDCABlockType($(this))){
        if(checkForRowSettings($(this))){
            //if(!getPopupFirstChild($(this))) handlePopupMessage($(this) ,'First Block Must Be A Row Settings Block');
        }else{
            //handlePopupMessage($(this) ,'Missing Row Settings');
            waitingForRowSettingsBlock = true;
        }
        listenForPopupSave($(this));
    }
});
}
//We make sure the popup entry has a DCA block
function validateDCABlockType(blockToCheck){
let returnVal = false;
let blockType = $(blockToCheck).attr('id');
if(blockType != null) {
    blockType = blockType.match(/(fields-)(.*)/gi);
    for (var val of dcaBlocks){//dcaHandle
        if(blockType[0] === 'fields-'+val.matrixHandle) returnVal = true;
    }
}
return returnVal;
}
//Displays the error message and hides the save button
function handlePopupMessage(parentMatrix, message){
let parent = $(parentMatrix).parent();
if($(parent).children('#missing').length == 0) $(parentMatrix).before('<span id="missing">'+message+'</span>');
else $(parent).children('#missing').text(message).fadeIn();
$(parent).children('#missing').css('color', '#ff6060');
}
//Checks if the first block is a row settings block
function getPopupFirstChild(matrixToCheck){
let returnVal = false;
//Gets the first block
let firstBlock = $(matrixToCheck).find('.matrixblock').first();
//Gets the data type: dcaRowSettings
let attr = firstBlock.attr('data-type');
//Get the ID of our matrix block: fields-dca
let blockType = $(matrixToCheck).attr('id');
blockType = blockType.match(/(fields-)(.*)/gi);
for (var val of dcaBlocks){//rowSettings
    if(blockType != null){
        if(blockType[0] == "fields-"+val.matrixHandle){//Our val in the loop matches our matrix handle
            let tempVal = false;
            let requiresFirstSlot = false;
            for(var val2 of val.blocks){
                if(val2.requireFirstSlot){//This block must be in the first slot
                    if(attr == val2.blockHandle) returnVal = true;//If the data type of our first block matches the current loops handle we're good
                    else {
                        requiresFirstSlot = true; //We didn't match but we require the first slot
                        handlePopupMessage(matrixToCheck, "A " + val2.blockName + " Block Must Be On Top");
                    }
                }
            }
            if (requiresFirstSlot && !returnVal) returnVal = false;//One block requires first slot but wasn't given it
        }
    }
}
return returnVal;
}
//Make sure we can save
//A row settings block must be in the matrix
//and it must be in the first slot
function popupValidator(matrixToCheck){
if($(matrixToCheck).children('.blocks').children().length === 0) return true;
else if(getPopupFirstChild(matrixToCheck) && checkForRowSettings(matrixToCheck)) return true;
else if (!checkForRowSettings(matrixToCheck)) return false;
else if(!getPopupFirstChild(matrixToCheck)) return false
else return false;
};

///////////////////////////////////// Entry Page Editor \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
//When the user clicks on the save button, we need to check if it should be prevented
$('#header .btngroup input').on('click', function(){
let returnVal = true;
if(foundDCA){
    let matrixToCheck = $('.matrix.matrix-field');
    if($(matrixToCheck).children('.blocks').children().length === 0) return true;
    else if(checkForRowSettings(matrixToCheck) && getFirstChild()) returnVal = true;
    else if(!checkForRowSettings(matrixToCheck)) returnVal = false;
    else if(!getFirstChild()) returnVal = false;
    else returnVal = false;
}else{
    returnVal = true;
}
return returnVal;
});
// Checking for dca fields
//Runs on page load
//Used for entry pages
let waitingForRowSettingsBlock = false;
let foundDCA = false;
for (var val of dcaBlocks){//matrix
if($('#fields-'+val.matrixHandle+'-field').length != 0){
    //There is a DCA field here
    foundDCA = true;
}
}
//Gets the first child and check if it's a row setting block
function getFirstChild(){
let returnVal = false;
//We need to get our matrix block's field ID
let matrix = $('.matrix.matrix-field');
let matrixID = $(matrix).attr('id');//Returns field-ID
let firstBlock = $('.matrix.matrix-field .blocks div').first();//Gets the first div
let dataType = $(firstBlock).attr('data-type');
for (var val of dcaBlocks){//Loop through all of our block settings
    if("fields-"+val.matrixHandle == matrixID){//This is the correct matrix type
        let tempVal = false;
            let requiresFirstSlot = false;
            for(var val2 of val.blocks){
                if(val2.requireFirstSlot){//This block must be in the first slot
                    if(dataType == val2.blockHandle) returnVal = true;//If the data type of our first block matches the current loops handle we're good
                    else {
                        requiresFirstSlot = true; //We didn't match but we require the first slot
                        handlePopupMessage(matrix, "A " + val2.blockName + " Block Must Be On Top");
                    }
                }
            }
            if (requiresFirstSlot && !returnVal) returnVal = false;//One block requires first slot but wasn't given it
    }
}
return returnVal;
}
//Checks if the matrix block has the required blocks
function checkForRowSettings(matrixToCheck){
let returnVal = false;
let tempArray = [];
let reqBlocks;
let blockType = $(matrixToCheck).attr('id');
blockType = blockType.match(/(fields-)(.*)/gi);
let matrixRequiredBlocksCount = 0;
for(var val of dcaBlocks){
    if(blockType[0] != null && blockType[0] == "fields-"+val.matrixHandle){//Found the matrix we want
        matrixRequiredBlocksCount = val.blocks.length;
        reqBlocks = val.blocks;
    }
}
$(matrixToCheck).find('.matrixblock').each(function(){
    let dataType = $(this).attr('data-type');//Gets data type of each block in the array
    for (var val of dcaBlocks){//loops through matrix
        if(blockType[0] != null && blockType[0] == "fields-"+val.matrixHandle){//Found the matrix we want
            for(var val2 of val.blocks){//loops through blocks
                //This is a block we require
                console.log("Found block");
                if(dataType == val2.blockHandle) tempArray.push(val2.blockName);
            }
        }
    }
});
if(tempArray.length >= matrixRequiredBlocksCount) returnVal = true;
else{
    //We're missing blocks!
    let missingBlockName = [];
    for(var x of reqBlocks){
        //Loop through all of our required blocks
        let foundRequired = false;
        for(var y of tempArray){ if(x.blockName == y) foundRequired = true; }
        if(!foundRequired) missingBlockName.push(x.blockName);
    }
    let missingBlockNameString;
    if(missingBlockName.length > 1) missingBlockNameString = missingBlockName.join(" & ");
    else missingBlockNameString = missingBlockName[0];
    missingBlockNameString = missingBlockNameString + " Block(s) Required";
    handlePopupMessage(matrixToCheck, missingBlockNameString);
}
return returnVal;
}
//Validate blocks so people cant f*ck everything up
(function(){
for(var x of dcaBlocks){//loop matrix
    let oneRequired = false;
    for(var y of x.blocks){//loop blocks
        if(y.requireFirstSlot && !oneRequired) oneRequired = true;
        else if(y.requireFirstSlot && oneRequired) y.requireFirstSlot = false;
    }
}
})();