# Dynamic Row Settings Changelog

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

### [Unreleased Changes]
[Current unreleased](https://github.com/andrewk9/Dynamic-Row-Settings/compare/v0.0.0...master)

---

### [0.0.8] - 2018-05-31
#### Adds
- Base `DynamicsRowSettings.js` based on previous module

#### Changes
- Settings to ask if the block should be forced and what the human readable version of the block should be called

---

### [0.0.7] - 2018-05-31
#### Changes
- Fixed `Settings.php` to handle users input
- Switched to table for DCA data input

---

### [0.0.6] - 2018-05-31
#### Changes
- `Settings.php` now returns the rules for our new form variables

---

### [0.0.5] - 2018-05-31
#### Adds
- Adjusted `config.php` to listen for our new settings fields

---

### [0.0.4] - 2018-05-31
#### Adds
- Updates `/src/templates/settings.twig` to ask for the DCA handles and their row settings block handles

---

### [0.0.3] - 2018-05-31
#### Changes
- Updates composer.json to allow us to install the plugin from this GitHub repo

---

### [0.0.2] - 2018-05-31
#### Changes
- Generated new plugin, the vendor dir was screwed up

---

### [0.0.1] - 2018-05-31
#### Changes
- Updates README
